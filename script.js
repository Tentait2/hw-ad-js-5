"use strict"
const userURL = 'https://ajax.test-danit.com/api/json/users'
const postsURL = 'https://ajax.test-danit.com/api/json/posts'
const divRoot = document.querySelector('#root')


class Requests {
    get(url) {
        return fetch(url).then((response) => response.json());
    }
}


class Card{
    constructor() {
        this.request = new Requests()
    }
    getPosts(){
        return this.request.get(postsURL).then(data =>{
            return data
        })
    }
    getUsers(){
        return this.request.get(userURL).then(data =>{
            return data
        })
    }
    deletePost(id) {
        return fetch(`https://ajax.test-danit.com/api/json/posts/${id}`, {
            method: "delete"
        })
    }


    createPost(userName, userEmail, title, body, id){
        const listItem = document.createElement("li")
        listItem.classList.add("post")
        listItem.insertAdjacentHTML("beforeend",
            `
                    <h2>${userName}</h2>
                    <p>${userEmail}</p>
                    <h2>${title}</h2>
                    <p>${body}</p>
                `
        )
        const deleteBtn = document.createElement("button")
        deleteBtn.textContent = "Delete Post"
        deleteBtn.addEventListener("click", ()=> {
            this.deletePost(id).then(() => {
                try {
                    listItem.style.display = "none"
                } catch (err) {
                    console.log(err);
                }
            })
        })

        listItem.append(deleteBtn)
        return listItem
    }
    renderPosts() {
        const list = document.createElement("ul")
        this.getPosts()
            .then((dataPost => {
                dataPost.map(({userId, title, body, id}) => {
                    this.getUsers()
                        .then(dataUsers => {
                            dataUsers.find(userItem => {
                                if(userItem.id === userId) {
                                    list.append(this.createPost(userItem.name, userItem.email, title, body, id))
                                }
                            })
                        })
                })
            }))
        return list
    }
}

const posts = new Card()
divRoot.append(posts.renderPosts())